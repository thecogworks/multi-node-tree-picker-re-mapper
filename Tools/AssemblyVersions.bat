@echo off
REM Ensure version number is given
IF .%1 == . (
	@echo off
	set /p VERSION="Enter version (eg. 1.0.2): " %=%
	@echo on
)
IF NOT .%1 == . (
	set VERSION=%1
)

REM ***********************************
REM Set version number for assemblies.
REM ***********************************


@echo off
REM ..\Tools\AssemblyInfoUtil\AssemblyInfoUtil.exe -set:%VERSION%.* "..\Source\Application.BusinessLogic\Properties\AssemblyInfo.cs
REM ..\Tools\AssemblyInfoUtil\AssemblyInfoUtil.exe -set:%VERSION%.* "..\Source\Application.Common\Properties\AssemblyInfo.cs
REM ..\Tools\AssemblyInfoUtil\AssemblyInfoUtil.exe -set:%VERSION%.* "..\Source\Application.Config\Properties\AssemblyInfo.cs

..\Tools\AssemblyInfoUtil\AssemblyInfoUtil.exe -set:%VERSION%.* "..\Source\Application.Web\Properties\AssemblyInfo.cs


exit /b
