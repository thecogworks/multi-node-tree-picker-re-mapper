REM -- Run to add required URL's to the local machine's hosts file during setup. ---
REM -- If you see a message stating "Access is denied." then you should run this using administrator access rights.

SET HOSTS=%WINDIR%\System32\drivers\etc\hosts
SET LOCALHOST=127.0.0.1
SET SITE=solution-template.local.tcwdigital.co.uk

cls
 
REM call function to add url
call:addToHosts %SITE% default

echo Completed!
GOTO End




:addToHosts - A simple function for adding to the hosts file
FIND /C /I %~1 %HOSTS%
IF %ERRORLEVEL% NEQ 0 (
	ECHO %NEWLINE%^%LOCALHOST%	%~1			#%~2 >>%HOSTS%
)


:End