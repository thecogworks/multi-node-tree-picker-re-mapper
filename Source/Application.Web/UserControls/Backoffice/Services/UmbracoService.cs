using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Models;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace Cogworks.MntpReMapper.Web.UserControls.Backoffice.Services
{

    public interface IUmbracoService
    {
        void GetMntpProperties(List<string> properties, List<string> contentTypes);
        IContent GetRoot(string rootName);
        IEnumerable<string> GetSites();
        List<IContent> GetDescendantsAndSelf(string rootName);
        List<Property> GetProperties(PropertyCollection properties, List<string> propertyAliases);

        List<int> GetNodesInCurrentTree(List<int> nodeIds, MappingModelContainer mappingContainer);
        List<MntpItem> GetMntpItemsInCurrentTree(List<int> nodeIds, MappingModelContainer mappingContainer);
    }


    public class UmbracoService : IUmbracoService
    {
        private readonly IContentService _contentService;
        private readonly IContentTypeService _contentTypeService;


        public UmbracoService(IContentService contentService, IContentTypeService contentTypeService)
        {
            _contentService = contentService;
            _contentTypeService = contentTypeService;
        }

        /// <summary>
        /// Gets nodes based on composite key in MappingModel.
        /// </summary>
        /// <returns></returns>
        public List<int> GetNodesInCurrentTree(List<int> nodeIds, MappingModelContainer mappingContainer)
        {
            var newIdsList = new List<int>();

            // get the nodes from the original tree by id
            foreach (var originalNodeId in nodeIds)
            {
                if (!mappingContainer.OriginalModels.ContainsKey(originalNodeId))
                {
                    // add the id back to the list, otherwise we will end up nuking the entire value when the tool is run more than once!
                    newIdsList.Add(originalNodeId);
                    continue;
                }

                // get master node
                var masterNode = mappingContainer.OriginalModels[originalNodeId];

                // get original node
                var currentNode = mappingContainer.CurrentModels[masterNode.CompositeKey];
                newIdsList.Add(currentNode.Id);
            }

            return newIdsList;
        }


        /// <summary>
        /// Gets nodes based on composite key in MappingModel.
        /// </summary>
        /// <returns></returns>
        public List<MntpItem> GetMntpItemsInCurrentTree(List<int> nodeIds, MappingModelContainer mappingContainer)
        {
            var mntpItems = new List<MntpItem>();
            var mntpItemsNotFound = new List<MntpItem>();

            // get the nodes from the original tree by id
            foreach (var originalNodeId in nodeIds)
            {
                if (!mappingContainer.OriginalModels.ContainsKey(originalNodeId))
                {
                    // add the id back to the list, otherwise we will end up nuking the entire value when the tool is run more than once!
                    mntpItems.Add(new MntpItem { Id = originalNodeId });
                    continue;
                }

                // get master node
                var masterNode = mappingContainer.OriginalModels[originalNodeId];

                // BF: Ensure the currentModel contains the composite key
                if (!mappingContainer.CurrentModels.ContainsKey(masterNode.CompositeKey))
                {
                    mntpItemsNotFound.Add(new MntpItem { Id = masterNode.Id, Name = masterNode.NodeName, Path = masterNode.CompositeKey });

                    continue;
                }

                // get current node information for status panel
                var currentNode = mappingContainer.CurrentModels[masterNode.CompositeKey];
                mntpItems.Add(new MntpItem { Id = currentNode.Id, Name = currentNode.NodeName, Path = masterNode.CompositeKey });
            }

            if (mntpItemsNotFound.Any())
            {
                var cachedItemsNotFound = (List<MntpItem>)HttpContext.Current.Session["MntpItemsNotFound"] ?? new List<MntpItem>();
                mntpItemsNotFound.AddRange(mntpItemsNotFound);

                HttpContext.Current.Session["MntpItemsNotFound"] = cachedItemsNotFound;
            }

            return mntpItems;
        }


        /// <summary>
        /// Gets properties and contentTypes which have MNTP's
        /// </summary>
        public void GetMntpProperties(List<string> properties, List<string> contentTypes)
        {
            // get all doctypes
            var allContentTypes = _contentTypeService.GetAllContentTypes();

            foreach (var contentType in allContentTypes)
            {
                // get mntp properties
                var propertyTypeAliases = contentType.PropertyTypes.Where(x => x.DataTypeId.ToString() == "7e062c13-7c41-4ad9-b389-41d88aeef87c").Select(x => x.Alias).ToList();

                properties.AddRange(propertyTypeAliases);

                if (propertyTypeAliases.Any())
                {
                    // save doctype alias
                    contentTypes.Add(contentType.Alias);
                }
            }
        }


        /// <summary>
        /// Gets root.
        /// </summary>
        /// <param name="rootName"></param>
        /// <returns></returns>
        public IContent GetRoot(string rootName)
        {
            // get all nodes
            var root = _contentService.GetByLevel(1).Single(x => x.Name.ToLower() == rootName.ToLower());

            return root;
        }

        /// <summary>
        /// Gets root.
        /// </summary>
        /// <param name="rootName"></param>
        /// <returns></returns>
        public IEnumerable<string> GetSites()
        {
            // get all nodes
            var sites = _contentService.GetByLevel(1).OrderBy(n => n.Id).Select(x => x.Name);

            return sites;
        }


        /// <summary>
        /// Gets descendants of the given types
        /// </summary>
        /// <returns></returns>
        public List<IContent> GetDescendantsAndSelf(string rootName)
        {
            var root = GetRoot(rootName);

            var nodes = _contentService.GetDescendants(root).ToList();
            nodes.Insert(0, root);
            return nodes;
        }


        /// <summary>
        /// Get properties that exist in propertyAliases.
        /// </summary>
        /// <param name="properties"></param>
        /// <param name="propertyAliases"></param>
        /// <returns></returns>
        public List<Property> GetProperties(PropertyCollection properties, List<string> propertyAliases)
        {
            var mntpProperties = properties.Where(x => propertyAliases.Contains(x.Alias));

            return mntpProperties.ToList();
        }


    }
}