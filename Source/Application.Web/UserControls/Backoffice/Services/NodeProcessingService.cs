﻿using System.Collections.Generic;
using System.Linq;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Extensions;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Models;
using Umbraco.Core.Models;

namespace Cogworks.MntpReMapper.Web.UserControls.Backoffice.Services
{
    public interface INodeProcessingService
    {
        List<MntpItem> CompareNode(IContent node, MappingModelContainer mappingContainer, List<string> propertyAliases);
        bool ProcessNode(IContent node, MappingModelContainer mappingContainer, List<string> propertyAliases);
    }


    public class NodeProcessingService : INodeProcessingService
    {
        private readonly IUmbracoService _UmbracoService;

        public NodeProcessingService(IUmbracoService UmbracoService)
        {
            _UmbracoService = UmbracoService;
        }

        /// <summary>
        /// Iterates over node MNTP properties.
        /// </summary>
        public List<MntpItem> CompareNode(IContent node, MappingModelContainer mappingContainer, List<string> propertyAliases)
        {
            var documentPickedItems = new List<MntpItem>();

            // get properties from node
            var properties = _UmbracoService.GetProperties(node.Properties, propertyAliases);

            foreach (var p in properties)
            {
                // get node ids from mntp
                var nodeIds = p.Value.ToString();

                var pickerItems = _UmbracoService.GetMntpItemsInCurrentTree(nodeIds.ToIntList(), mappingContainer);

                // Extract a list of Node Ids from the collection, for comparison to the original
                var pickerNodeIds = pickerItems.Select(item => item.Id).ToList();
                var newValue = string.Join(",", pickerNodeIds);

                // If the value has not yet been updated
                if (nodeIds != newValue)
                {
                    // Add the picker items to the collection for the whole node
                    documentPickedItems.AddRange(pickerItems);
                }
            }

            return documentPickedItems;
        }


        /// <summary>
        /// Iterates over node MNTP properties.
        /// </summary>
        public bool ProcessNode(IContent node, MappingModelContainer mappingContainer, List<string> propertyAliases )
        {
            bool hasChanged = false;

            // get properties from node
            var properties = _UmbracoService.GetProperties(node.Properties, propertyAliases);

            foreach (var p in properties)
            {
                // get node ids from mntp
                var nodeIds = p.Value.ToString().ToIntList();

                var pickerItems = _UmbracoService.GetNodesInCurrentTree(nodeIds, mappingContainer);

                // save the new values
                var newValue = string.Join(",", pickerItems);

                node.SetValue(p.Alias, newValue);

                hasChanged = true;
            }

            return hasChanged;
        }
    }
}