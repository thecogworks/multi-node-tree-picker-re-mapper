using System;
using System.Collections.Generic;
using System.Linq;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Models;
using umbraco.cms.presentation;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace Cogworks.MntpReMapper.Web.UserControls.Backoffice.Services
{
    public class MappingService
    {
        private readonly IContentService _contentService;
        private readonly INodeProcessingService _nodeProcessingService;

        public MappingService(IContentService contentService, INodeProcessingService nodeProcessingService)
        {
            _contentService = contentService;
            
            _nodeProcessingService = nodeProcessingService;
        }


        /// <summary>
        /// Gets models based on entire tree. This is very expensive, and could be refactored to cache ancestor results based on ID path.
        /// </summary>
        /// <param name="masterNodes"></param>
        /// <returns></returns>
        [Obsolete]
        public List<MappingModel> GetMappingModels(IEnumerable<IContent> masterNodes)
        {
            var models = new List<MappingModel>();
            foreach (var masterNode in masterNodes)
            {
                var model = new MappingModel()
                {
                    Id = masterNode.Id,
                    NodeName = masterNode.Name,
                    DocumentTypeAlias = masterNode.ContentType.Alias,
                };

                var ancestors = _contentService.GetAncestors(masterNode.Id).ToList();
                ancestors.Add(masterNode); // current node to end of list

                var ancestorStrings = ancestors.Select(x => string.Format("{0}({1})", x.Name, x.ContentType.Alias)).ToList();

                if (ancestorStrings.Any())
                {
                    ancestorStrings[0] = "(ROOT)";
                }                
                model.CompositeKey = string.Join("-", ancestorStrings);

                models.Add(model);
            }
            return models;
        }


        /// <summary>
        /// Traverses tree to create mapping models
        /// </summary>
        /// <returns></returns>
        public List<MappingModel> GetMappingModels(IContent root, List<IContent> nodes)
        {
            // dictionary to store paths for quick lookup
            var pathDictionary = new Dictionary<string, string>();
            var models = new List<MappingModel>();

            // use a queue to traverse the tree
            var q = new Queue<IContent>();
            q.Enqueue(root);
            while (q.Any())
            {
                // pop queue
                var current = q.Dequeue();
                var model = new MappingModel()
                {
                    Id = current.Id,
                    NodeName = current.Name,
                    DocumentTypeAlias = current.ContentType.Alias,
                };

                if (current.Id == root.Id)
                {
                    model.CompositeKey = "(ROOT)";
                }
                else
                {
                    // get path without current node
                    var parentPathList = current.Path.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList().Where(x=>x!= current.Id.ToString());
                    var parentPath = string.Join(",", parentPathList);

                    // get name path
                    var parentNamePath = pathDictionary[parentPath];

                    model.CompositeKey = string.Format("{0}-{1}({2})", parentNamePath, current.Name, current.ContentType.Alias);
                }

                // save 
                models.Add(model);
                pathDictionary.Add(current.Path, model.CompositeKey);
                nodes.Add(current);

                // add children to queue
                _contentService.GetChildren(current.Id).ForEach(q.Enqueue);
            }

            return models;
        }


        /// <summary>
        /// Does the comparison.
        /// </summary>
        public List<DocumentWithMntp> CompareMntpProperties(List<IContent> nodesToProcess, List<string> contentTypeAliases, List<string> propertyAliases, MappingModelContainer mappingContainer)
        {
            var nodesToMap = new List<DocumentWithMntp>();

            foreach (var node in nodesToProcess)
            {
                if (NotMntpField(contentTypeAliases, node))
                {
                    // so we don't process a node which doesnt have a MNTP 
                    continue;
                }

                var mntpItems = _nodeProcessingService.CompareNode(node, mappingContainer, propertyAliases);
                nodesToMap.Add(new DocumentWithMntp {Id = node.Id, Name = node.Name, MntpItems = mntpItems } );
            }

            return nodesToMap;
        }


        /// <summary>
        /// Does the bulk mapping.
        /// </summary>
        //public void MapMntpProperties(List<string> properties, List<string> contentTypes)
        public void MapMntpProperties(List<IContent> nodesToProcess, List<string> contentTypeAliases, List<string> propertyAliases, MappingModelContainer mappingContainer)
        {
            foreach (var node in nodesToProcess)
            {
                if (NotMntpField(contentTypeAliases, node))
                {
                    // so we don't process a node which doesnt have a MNTP 
                    continue;
                }

                bool hasChanged = _nodeProcessingService.ProcessNode(node, mappingContainer, propertyAliases);
                if (hasChanged)
                {
                    _contentService.Save(node, 0, false);
                }
            }
        }

        private static bool NotMntpField(List<string> contentTypeAliases, IContent node)
        {
            return contentTypeAliases.Any(x => x == node.ContentType.Alias) == false;
        }
    }
}