﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MntpRemapper.ascx.cs" Inherits="Cogworks.MntpReMapper.Web.usercontrols.Backoffice.MntpRemapper" %>

Instructions: 
<br />
<div>1. Click to start</div>
<asp:Button ID="Button1" runat="server" Text="Load sites" OnClick="Button1_Click" />
<br />
<br />

<br />
<div>2. Select original root and destination root</div>
<asp:DropDownList ID="OriginalRootName" runat="server" placeholder="Original root name" Style="width: 150px"></asp:DropDownList>
<%--<asp:TextBox ID="TxtOriginalRootName" runat="server" placeholder="Original root name" Value="Site" Style="width: 150px" />--%>&nbsp;
<asp:DropDownList ID="RootName" runat="server" placeholder="Root node name to remap" Style="width: 150px"></asp:DropDownList>
<%--<asp:TextBox ID="TxtRootName" runat="server" placeholder="Root node name to remap" Value="Site (1)" Style="width: 150px" />--%><br />
<br />
<br />

<div>3. Compare differences!</div>
<asp:Button ID="btnCompare" runat="server" Text="Compare now" OnClick="btnCompare_Click" />
<br />
<br />
<asp:Label ID="lblCompareMessage" runat="server" Style="color: orangered !important; font-weight: bold; font-size: 18px;" />
<br />
<asp:Panel ID="pnlDoMapping" Visible="False" runat="server">

        <asp:Repeater ID="rptNodeList" runat="server">
        <HeaderTemplate>
            <table rules="rows" border="0" class="members_table">
                <thead>
                    <tr>
                        <th>Target node ID</th>
                        <th>Document name</th>
                        <th>Picked items to remap</th>
                        <th></th><th></th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%# Eval("ID") %></td>
                <td><%# Eval("Name") %></td>
                <td colspan="3">
                    <ul>
                        <asp:Repeater runat="server" DataSource='<%# Eval("MntpItems") %>'>
                            <ItemTemplate><li><%# Eval("Name") %> <em>(<%#Eval("Id")%>)</em>,  Path: <em><%# Eval("Path") %></em></li></ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </td>
            </tr>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <tr class="alt">
                <td><%# Eval("ID") %></td>
                <td><%# Eval("Name") %></td>
                <td colspan="3">
                    <ul>
                        <asp:Repeater runat="server" DataSource='<%# Eval("MntpItems") %>'>
                            <ItemTemplate><li><%# Eval("Name") %> <em>(<%#Eval("Id")%>)</em>,  Path: <em><%# Eval("Path") %></em></li></ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </td>
            </tr>
        </AlternatingItemTemplate>
        <FooterTemplate>
            </tbody>
	        </table>
        </FooterTemplate>
    </asp:Repeater>

    <br />
    <asp:Button ID="Button2" runat="server" Text="Do Mapping" OnClick="Button2_Click" />
    <br/>
    <br/>

</asp:Panel>

<asp:Label ID="lblMessage" runat="server" Style="color: green !important; font-weight: bold; font-size: 18px;" />


<asp:Panel ID="pnlErrors" Visible="False" runat="server">
    <h3>Issues to address</h3>
    <p>The following nodes require remapping but could not be matched in the original tree:</p>
    <ul>
        <asp:Repeater ID="ErrorList" runat="server">
            <ItemTemplate><li><%# Eval("Name") %> <em>(<%#Eval("Id")%>)</em>,  Path: <em><%# Eval("Path") %></em></li></ItemTemplate>
        </asp:Repeater>
    </ul>
</asp:Panel>


<br />
<br />
<br />
Doctypes<br />
<asp:TextBox ID="TxtContentTypeAliases" runat="server" placeholder="Doctype aliases with MNTP" TextMode="MultiLine" Style="width: 300px; height: 370px;" />
<br />
<br />
Properties<br />
<asp:TextBox ID="TxtPropertyAliases" runat="server" placeholder="Property aliases using MNTP" TextMode="MultiLine" Style="width: 300px; height: 350px;" />




