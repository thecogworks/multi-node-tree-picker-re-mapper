using System;
using System.Collections.Generic;
using System.Linq;

namespace Cogworks.MntpReMapper.Web.UserControls.Backoffice.Extensions
{
    public static class StringExtensions
    {
        public static List<int> ToIntList(this string value)
        {
            return value.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Trim())
                .Select(int.Parse)
                .ToList();
        }
    }
}