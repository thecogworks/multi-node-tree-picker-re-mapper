﻿using Cogworks.MntpReMapper.Web.usercontrols.Backoffice;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Services;
using Umbraco.Core;

namespace Cogworks.MntpReMapper.Web.UserControls.Backoffice.Factories
{
    public class ServiceFactory
    {
        public static IUmbracoService GetUmbracoService()
        {
            var UmbracoService = new UmbracoService(ApplicationContext.Current.Services.ContentService, ApplicationContext.Current.Services.ContentTypeService);
            return UmbracoService;
        }


        public static MappingService GetMappingService()
        {
            var processor = GetNodeProcessingService();
            var mappingService = new MappingService(ApplicationContext.Current.Services.ContentService, processor);
            return mappingService;
        }


        public static INodeProcessingService GetNodeProcessingService()
        {
            var processor = new NodeProcessingService(GetUmbracoService());
            return processor;
        }
    }
}