﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Factories;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Models;
using Umbraco.Core;
using Umbraco.Core.Models;

namespace Cogworks.MntpReMapper.Web.usercontrols.Backoffice
{
    public partial class MntpRemapper : System.Web.UI.UserControl
    {
        private UserControls.Backoffice.Services.MappingService _mappingService;
        private UserControls.Backoffice.Services.IUmbracoService _umbracoService;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            _umbracoService = ServiceFactory.GetUmbracoService();
            _mappingService = ServiceFactory.GetMappingService();
            
            //if (!Page.IsPostBack)
            //{
            //    _umbracoService = ServiceFactory.GetUmbracoService();
            //    _mappingService = ServiceFactory.GetMappingService();
            //}
        }

        /// <summary>
        /// Gets aliases.
        /// </summary>
        protected void Button1_Click(object sender, EventArgs e)
        {
            var propertyAliases = new List<string>();
            var contentTypeAliases = new List<string>();

            _umbracoService.GetMntpProperties(propertyAliases, contentTypeAliases);

            TxtPropertyAliases.Text = string.Join("\n", propertyAliases.Distinct());
            TxtContentTypeAliases.Text = string.Join("\n", contentTypeAliases);

            OriginalRootName.DataSource = RootName.DataSource = _umbracoService.GetSites();
            OriginalRootName.DataBind();
            RootName.DataBind();
        }

        protected void btnCompare_Click(object sender, EventArgs e)
        {

            var originalRoot = _umbracoService.GetRoot(OriginalRootName.SelectedValue);
            var currentRoot = _umbracoService.GetRoot(RootName.SelectedValue);

            var originalNodes = new List<IContent>();
            var currentNodes = new List<IContent>();

            // map the nodes to models
            var masterModels = _mappingService.GetMappingModels(originalRoot, originalNodes);
            var currentModels = _mappingService.GetMappingModels(currentRoot, currentNodes);

            // create dictionaries
            var masterDic = masterModels.ToDictionary(x => x.Id, x => x);
            var currentDic = currentModels.ToDictionary(x => x.CompositeKey, x => x);

            var contentTypeAliases = TxtContentTypeAliases.Text.Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();
            var propertyAliases = TxtPropertyAliases.Text.Split("\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();

            var container = new MappingModelContainer()
            {
                OriginalModels = masterDic,
                CurrentModels = currentDic
            };

            // do compare
            var documentsToUpdate = _mappingService.CompareMntpProperties(currentNodes, contentTypeAliases, propertyAliases, container);

            // Cache items so they persist across postbacks
            Session["CurrentNodes"] = currentNodes;
            Session["ContentTypeAliases"] = contentTypeAliases;
            Session["PropertyAliases"] = propertyAliases;
            Session["Container"] = container;

            if (documentsToUpdate.Count > 0)
            {
                ShowMappingInfo(documentsToUpdate);

            }
            
        }


        protected void Button2_Click(object sender, EventArgs e)
        {
            // Check session 
            if (Session["CurrentNodes"] == null || string.IsNullOrEmpty(Session["CurrentNodes"].ToString()))
            {
                // Create session vars if timed out
                btnCompare_Click(sender, e);
            }
            else
            {
                // Retrieve items from Cache
                var currentNodes = (List<IContent>)Session["CurrentNodes"];
                var contentTypeAliases = (List<string>)Session["ContentTypeAliases"];
                var propertyAliases = (List<string>)Session["PropertyAliases"];
                var container = (MappingModelContainer)Session["Container"];

                // do bulk mapping
                _mappingService.MapMntpProperties(currentNodes, contentTypeAliases, propertyAliases, container);

                // redo compare
                var documentsToUpdate = _mappingService.CompareMntpProperties(currentNodes, contentTypeAliases, propertyAliases, container);
                ShowMappingInfo(documentsToUpdate);

                // Check for errors
                var mntpItemsNotFound = (List<MntpItem>)HttpContext.Current.Session["MntpItemsNotFound"] ?? new List<MntpItem>();
                if (mntpItemsNotFound.Any())
                {
                    ErrorList.DataSource = mntpItemsNotFound;
                    ErrorList.DataBind();

                    pnlErrors.Visible = true;
                }

            }
        }


        private void ShowMappingInfo(IEnumerable<DocumentWithMntp> documentsToUpdate)
        {
            lblMessage.Text = lblCompareMessage.Text = string.Empty;
            
            // Parse the collection of documents to see how many MNTP nodes need mapping
            var documentsWithMntp = documentsToUpdate as DocumentWithMntp[] ?? documentsToUpdate.ToArray();
            var mntpNodesToMap = documentsWithMntp.SelectMany(c => c.MntpItems).Count();

            if (mntpNodesToMap > 0)
            {
                // Bind repeater
                rptNodeList.DataSource = documentsToUpdate;
                rptNodeList.DataBind();

                // Feedback
                lblCompareMessage.Text = string.Format("Found {0} picked nodes in {1} documents that need remapping:<br />", mntpNodesToMap, documentsWithMntp.Count());
                pnlDoMapping.Visible = true;
            }
            else
            {
                lblMessage.Text = "No nodes found that require remapping.  How about a cup of tea?";
                pnlDoMapping.Visible = false;
            }
        }

        
    }
}