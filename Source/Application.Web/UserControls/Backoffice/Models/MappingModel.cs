﻿using System.Collections.Generic;

namespace Cogworks.MntpReMapper.Web.UserControls.Backoffice.Models
{
    public class MappingModelContainer
    {
        public Dictionary<int, MappingModel> OriginalModels { get; set; }
        public Dictionary<string, MappingModel> CurrentModels { get; set; }

        public Dictionary<int, int> Mappings = new Dictionary<int, int>();
    }



    public class MappingModel
    {
        public int Id { get; set; }
        public string NodeName { get; set; }
        public string DocumentTypeAlias { get; set; }


        public string CompositeKey { get; set;
           
        }
    }


    public class MntpItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }

    public class DocumentWithMntp
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<MntpItem> MntpItems { get; set; }
    }
}