﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;


namespace Application.Config
{

    public class SampleConfig : ConfigurationSection
    {
        [ConfigurationProperty("SampleItem", IsRequired = true)]
        public string SampleItem
        {
            get
            {
                return (string)this["SampleItem"];
            }
        }

    }
}
