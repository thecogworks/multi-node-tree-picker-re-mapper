﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Application.BusinessLogic.Services;

namespace Application.BusinessLogic.Factories
{
    public class SampleFactory
    {
        public static ISampleService GetSampleService()
        {
            return new SampleService();
        }

    }
}
