﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Models;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Services;
using NUnit.Framework;
using Moq;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace Application.Test
{
    [TestFixture]
    public class UmbracoServiceTests
    {
        private Mock<IContentService> _contentServiceMock;
        private Mock<IContentTypeService> _contentTypeServiceMock;

        private UmbracoService _umbracoService;

        [TestFixtureSetUp]
        public void Setup()
        {
            _contentServiceMock = new Mock<IContentService>();
            _contentTypeServiceMock = new Mock<IContentTypeService>();
            _umbracoService = new UmbracoService(_contentServiceMock.Object, _contentTypeServiceMock.Object);
        }



        [Test]
        public void GetMntpProperties_Sets_Lists()
        {
            // setup IContent to process
            var contentTypeMock = new Mock<IContentType>();
            contentTypeMock.Setup(x => x.Alias).Returns("alias");

            // setup property types
            var dataTypeDefinitionMock = new Mock<IDataTypeDefinition>();
            dataTypeDefinitionMock.Setup(x => x.ControlId).Returns(Guid.Parse("7e062c13-7c41-4ad9-b389-41d88aeef87c"));

            var propertyType = new PropertyType(dataTypeDefinitionMock.Object) { Alias = "propertyTypeAlias" };
            var propertyTypes = new List<PropertyType>() { propertyType };

            contentTypeMock.Setup(x => x.PropertyTypes).Returns(propertyTypes);

            var contentTypeList = new List<IContentType>()
            {
                contentTypeMock.Object
            };

            _contentTypeServiceMock.Setup(x => x.GetAllContentTypes()).Returns(contentTypeList);

            var properties = new List<string>();
            var contentTypes = new List<string>();
            _umbracoService.GetMntpProperties(properties, contentTypes);

            Assert.AreEqual("propertyTypeAlias", properties.First());
            Assert.AreEqual("alias", contentTypes.First());
        }



        [Test]
        public void GetRoot_Returns_Node()
        {
            // setup IContent to process
            var contentMock = new Mock<IContent>();
            contentMock.Setup(x => x.Name).Returns("node");
            var contentList = new List<IContent>()
            {
                contentMock.Object
            };

            _contentServiceMock.Setup(x => x.GetByLevel(1)).Returns(contentList);
            var res = _umbracoService.GetRoot("NODE");

            Assert.AreEqual(contentMock.Object, res);
        }



        [Test]
        public void GetDescendants_Returns_Nodes()
        {
            // setup IContent to process
            var contentMock = new Mock<IContent>();
            contentMock.Setup(x => x.Name).Returns("node");
            // set up descendants
            var contentDescedantMock = new Mock<IContent>();
            contentDescedantMock.Setup(x => x.Name).Returns("node");

            // setup IContent to process
            var descendantContentTypeMock = new Mock<IContentType>();
            descendantContentTypeMock.Setup(x => x.Alias).Returns("alias");

            contentDescedantMock.Setup(x => x.ContentType).Returns(descendantContentTypeMock.Object);
            var contentDescedantList = new List<IContent>()
            {
                contentDescedantMock.Object
            };

            _contentServiceMock.Setup(x => x.GetDescendants(contentMock.Object)).Returns(contentDescedantList);

            var contentList = new List<IContent>()
            {
                contentMock.Object
            };

            _contentServiceMock.Setup(x => x.GetByLevel(1)).Returns(contentList);

            var res = _umbracoService.GetDescendantsAndSelf("NODE");

            Assert.AreEqual(contentMock.Object, res.First());
            Assert.AreEqual(contentDescedantMock.Object, res.Last());
        }



        [Test]
        public void GetProperties_Returns_Property()
        {
            // setup properties
            var dataTypeDefinitionMock = new Mock<IDataTypeDefinition>();

            var properties = new List<Property>()
            {
                new Property(
                    new PropertyType(dataTypeDefinitionMock.Object)
                    {
                        Alias = "alias"
                    }
                ),
                new Property(
                    new PropertyType(dataTypeDefinitionMock.Object)
                    {
                        Alias = "alias2"
                    }
                )
            };


            var propertiesCollection = new PropertyCollection(properties);

            var propertyAliases = new List<string>() { "alias" };

            var res = _umbracoService.GetProperties(propertiesCollection, propertyAliases);

            Assert.AreEqual(1, res.Count());
            Assert.AreEqual("alias", res.First().Alias);

        }


        [Test]
        public void GetNodesInCurrentTree_Returns_Property()
        {
            var nodeIds = new List<int>() { 1, 2, 3 };

            // setup mapping models
            var originalModel = new MappingModel()
            {
                Id = 1,
                CompositeKey = "key"
            };

            var currentModel = new MappingModel()
            {
                Id = 888,
                CompositeKey = "key"
            };

            var mappingContainer = new MappingModelContainer();
            mappingContainer.OriginalModels = new Dictionary<int, MappingModel>()
            {
                { originalModel.Id, originalModel }
            };

            mappingContainer.CurrentModels = new Dictionary<string, MappingModel>()
            {
                { currentModel.CompositeKey, currentModel }
            };


            var res = _umbracoService.GetNodesInCurrentTree(nodeIds, mappingContainer);

            Assert.AreEqual(888, res.First());
        }
    }
}
