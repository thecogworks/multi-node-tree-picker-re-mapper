﻿using System.Collections.Generic;
using System.Linq;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Models;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Services;
using NUnit.Framework;
using Moq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace Application.Test
{
    [TestFixture]
    public class MappingServiceTests
    {
        private Mock<IContentService> _contentServiceMock;
        private Mock<INodeProcessingService> _nodeProcessingServiceMock;

        private MappingService _mntpMapper;
        private List<string> _contentTypeAliases;

        [TestFixtureSetUp]
        public void Setup()
        {
            _contentServiceMock = new Mock<IContentService>();
            _nodeProcessingServiceMock = new Mock<INodeProcessingService>();

            _mntpMapper = new MappingService(_contentServiceMock.Object, _nodeProcessingServiceMock.Object);
        }

        [Test]
        public void GetMappingModels_Creates_CompositeKey_From_Ancestors()
        {
            // setup IContent to process
            var contentTypeMock = new Mock<IContentType>();
            contentTypeMock.Setup(x => x.Alias).Returns("alias");

            var contentMock = new Mock<IContent>();
            contentMock.Setup(x => x.Id).Returns(1);
            contentMock.Setup(x => x.Name).Returns("node");
            contentMock.Setup(x => x.ContentType).Returns(contentTypeMock.Object);

            // set up ancestor nodes for tree traversal
            var parentMock = new Mock<IContent>();
            var grandParentMock = new Mock<IContent>();
            var rootMock = new Mock<IContent>();

            var parentContentTypeMock = new Mock<IContentType>();
            parentContentTypeMock.Setup(x => x.Alias).Returns("parent alias");
            var grandParentContentTypeMock = new Mock<IContentType>();
            grandParentContentTypeMock.Setup(x => x.Alias).Returns("grandParent alias");

            parentMock.Setup(x => x.Name).Returns("parent");
            parentMock.Setup(x => x.ContentType).Returns(parentContentTypeMock.Object);
            grandParentMock.Setup(x => x.Name).Returns("grandParent");
            grandParentMock.Setup(x => x.ContentType).Returns(grandParentContentTypeMock.Object);

            //(ROOT)
            //(ROOT)
            var rootContentTypeMock = new Mock<IContentType>();
            rootContentTypeMock.Setup(x => x.Alias).Returns("root alias");
            rootMock.Setup(x => x.Name).Returns("root");
            rootMock.Setup(x => x.ContentType).Returns(rootContentTypeMock.Object);


            // set up ancestors
            var ancestors = new List<IContent>() { rootMock.Object, grandParentMock.Object, parentMock.Object, };
            _contentServiceMock.Setup(x => x.GetAncestors(1)).Returns(ancestors);

            var list = new List<IContent>() { contentMock.Object };

            // finally do the test!
            var res = _mntpMapper.GetMappingModels(list);
            Assert.AreEqual(1, res.First().Id);
            Assert.AreEqual("alias", res.First().DocumentTypeAlias);
            Assert.AreEqual("(ROOT)-grandParent(grandParent alias)-parent(parent alias)-node(alias)", res.First().CompositeKey);
        }



        [Test]
        public void GetMappingModels_Creates_CompositeKey_Traversal()
        {
            // setup content types
            var contentTypeMock = new Mock<IContentType>();
            contentTypeMock.Setup(x => x.Alias).Returns("alias");

            var parentContentTypeMock = new Mock<IContentType>();
            parentContentTypeMock.Setup(x => x.Alias).Returns("parent alias");

            var grandParentContentTypeMock = new Mock<IContentType>();
            grandParentContentTypeMock.Setup(x => x.Alias).Returns("grandParent alias");

            // setup nodes
            var contentMock = new Mock<IContent>();
            var parentMock = new Mock<IContent>();
            var grandParentMock = new Mock<IContent>();
            var rootMock = new Mock<IContent>();

            // setup leaf node
            contentMock.Setup(x => x.Id).Returns(4);
            contentMock.Setup(x => x.Path).Returns("-1,1,2,3,4");
            contentMock.Setup(x => x.Name).Returns("node");
            contentMock.Setup(x => x.ContentType).Returns(contentTypeMock.Object);
           
            // setup parent 
            parentMock.Setup(x => x.Id).Returns(3);
            parentMock.Setup(x => x.Path).Returns("-1,1,2,3");
            parentMock.Setup(x => x.Name).Returns("parent");
            parentMock.Setup(x => x.ContentType).Returns(parentContentTypeMock.Object);

            // setup grandparent
            grandParentMock.Setup(x => x.Id).Returns(2);
            grandParentMock.Setup(x => x.Path).Returns("-1,1,2");
            grandParentMock.Setup(x => x.Name).Returns("grandParent");
            grandParentMock.Setup(x => x.ContentType).Returns(grandParentContentTypeMock.Object);

            //(ROOT)
            //(ROOT)
            var rootContentTypeMock = new Mock<IContentType>();
            rootContentTypeMock.Setup(x => x.Alias).Returns("root alias");
            rootMock.Setup(x => x.Id).Returns(1);
            rootMock.Setup(x => x.Path).Returns("-1,1");
            rootMock.Setup(x => x.Name).Returns("root");
            rootMock.Setup(x => x.ContentType).Returns(rootContentTypeMock.Object);

            // setup children lists
            _contentServiceMock.Setup(x => x.GetChildren(It.Is<int>(i => i == rootMock.Object.Id))).Returns(new List<IContent>() { grandParentMock.Object });
            _contentServiceMock.Setup(x => x.GetChildren(It.Is<int>(i => i == grandParentMock.Object.Id))).Returns(new List<IContent>() { parentMock.Object });
            _contentServiceMock.Setup(x => x.GetChildren(It.Is<int>(i => i == parentMock.Object.Id))).Returns(new List<IContent>() { contentMock.Object });
            _contentServiceMock.Setup(x => x.GetChildren(It.Is<int>(i => i == contentMock.Object.Id))).Returns(new List<IContent>());


            var nodes = new List<IContent>();

            // finally do the test!
            var res = _mntpMapper.GetMappingModels(rootMock.Object, nodes);

            Assert.AreEqual(4, nodes.Count);

            Assert.AreEqual("(ROOT)", res[0].CompositeKey);
            Assert.AreEqual("(ROOT)-grandParent(grandParent alias)", res[1].CompositeKey);
            Assert.AreEqual("(ROOT)-grandParent(grandParent alias)-parent(parent alias)", res[2].CompositeKey);
            Assert.AreEqual("(ROOT)-grandParent(grandParent alias)-parent(parent alias)-node(alias)", res[3].CompositeKey);

            Assert.AreEqual(1, res[0].Id);
            Assert.AreEqual(2, res[1].Id);
            Assert.AreEqual(3, res[2].Id);
            Assert.AreEqual(4, res[3].Id);
        }


        [Test]
        public void MapMntpProperties_Calls_ProcessNode()
        {
            // setup IContent to process
            var contentMock = new Mock<IContent>();
            var contentTypeMock = new Mock<IContentType>();
            contentTypeMock.Setup(x => x.Alias).Returns("alias");
            contentMock.Setup(x => x.ContentType).Returns(contentTypeMock.Object);

            _contentServiceMock.Setup(x => x.Save(contentMock.Object, 0, false));

            _nodeProcessingServiceMock.Setup(x => x.ProcessNode(contentMock.Object, It.IsAny<MappingModelContainer>(), It.IsAny<List<string>>())).Returns(true);

            _contentTypeAliases = new List<string>() { "alias" };
            var list = new List<IContent>() { contentMock.Object };
            _mntpMapper.MapMntpProperties(list, _contentTypeAliases, new List<string>(), new MappingModelContainer());

            _nodeProcessingServiceMock.Verify(x => x.ProcessNode(contentMock.Object, It.IsAny<MappingModelContainer>(), It.IsAny<List<string>>()));
            _contentServiceMock.Verify(x => x.Save(contentMock.Object, 0, false));
        }


        [Test]
        public void MapMntpProperties_Does_Note_ProcessNode_Because_For_Alias_Does_Not_Exist_In_Allowed_Aliases()
        {
            // setup IContent to process
            var contentMock = new Mock<IContent>();
            var contentTypeMock = new Mock<IContentType>();
            contentTypeMock.Setup(x => x.Alias).Returns("alias");
            contentMock.Setup(x => x.ContentType).Returns(contentTypeMock.Object);

            _contentServiceMock.Setup(x => x.Save(contentMock.Object, 0, false));

            _nodeProcessingServiceMock.Setup(x => x.ProcessNode(contentMock.Object, It.IsAny<MappingModelContainer>(), It.IsAny<List<string>>())).Returns(true);

            _contentTypeAliases = new List<string>() { "someOtherAlias" };
            var list = new List<IContent>() { contentMock.Object };
            _mntpMapper.MapMntpProperties(list, _contentTypeAliases, new List<string>(), new MappingModelContainer());

            _nodeProcessingServiceMock.Verify(x => x.ProcessNode(contentMock.Object, It.IsAny<MappingModelContainer>(), It.IsAny<List<string>>()), Times.Never);
            _contentServiceMock.Verify(x => x.Save(contentMock.Object, 0, false), Times.Never);
        }
    }
}
