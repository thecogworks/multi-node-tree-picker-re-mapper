﻿using System.Collections.Generic;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Models;
using Cogworks.MntpReMapper.Web.UserControls.Backoffice.Services;
using NUnit.Framework;
using Moq;
using Umbraco.Core.Models;
using Property = Umbraco.Core.Models.Property;

namespace Application.Test
{
    [TestFixture]
    public class NodeProcessingServiceServiceTests
    {

        private Mock<IUmbracoService> _umbracoService;
        private NodeProcessingService _nodeProcessingService;


        [SetUp]
        public void Setup()
        {
            _umbracoService = new Mock<IUmbracoService>();
            _nodeProcessingService = new NodeProcessingService(_umbracoService.Object);
        }



        [Test]
        public void GetRoot_Returns_Node()
        {
            // setup IContent to process
            var contentMock = new Mock<IContent>();
            contentMock.Setup(x => x.Name).Returns("node");


            // setup properties
            var dataTypeDefinitionMock = new Mock<IDataTypeDefinition>();

            var properties = new List<Property>()
            {
                new Property(
                    new PropertyType(dataTypeDefinitionMock.Object)
                    {
                        Alias = "alias"
                    }, 
                    "1,2,3"
                )
            };

            var propertiesCollection = new PropertyCollection(properties);
            contentMock.Setup(x => x.Properties).Returns(propertiesCollection);

            var propertyAliases = new List<string>() { "alias", "alias2" };

            var mappingContainer = new MappingModelContainer();

            //setup properties from UmbracoService
            _umbracoService.Setup(x => x.GetProperties(propertiesCollection, propertyAliases)).Returns(properties);

            // setup get mapped nodes
            _umbracoService.Setup(x => x.GetNodesInCurrentTree(It.IsAny<List<int>>(), mappingContainer)).Returns(new List<int>() { 777, 888, 999 });

            // setup set value
            contentMock.Setup(x => x.SetValue(It.IsAny<string>(), "777,888,999"));

            var res = _nodeProcessingService.ProcessNode(contentMock.Object, mappingContainer, propertyAliases);

            contentMock.Verify(x => x.SetValue(It.IsAny<string>(), "777,888,999"));
            Assert.True(res);
        }


    }
}
